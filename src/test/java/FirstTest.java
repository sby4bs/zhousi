import com.thermoberg.zhousi.service.DeviceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by zhaoyou on 17/08/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("../webapp/WEB-INF/dispatcher-servlet.xml")
public class FirstTest {

    @Autowired
    private DeviceService deviceService;

    @Test
    public void test() {
        System.out.println(deviceService.findAll());
        System.out.println("hello world");
    }
}
