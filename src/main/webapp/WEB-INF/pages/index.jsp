<%--
  Created by IntelliJ IDEA.
  User: zhaoyou
  Date: 07/08/2017
  Time: 4:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>查询探头历史数据</title>

    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<% request.setAttribute("active","1");%>
<%@ include file="navbar.jsp"%>

<div class="container" >
    <div class="panel panel-default">
        <div class="panel-heading">
           <h5>探头数据</h5>
        </div>
        <div class="panel-body">
            <div class="row">
                <form class="form-horizontal" role="form">

                    <div class="form-group">
                        <label  class="col-sm-2 control-label">探头列表</label>
                        <div class="col-sm-8">
                            <c:forEach var="d" items="${deviceList}">
                                <label style="margin-right: 10px">
                                    <input type="radio" class="tagId" name="tagId" ${param.tagId == d.tagId ? 'checked' : ''} value="${d.tagId}">
                                        ${d.tagId}
                                </label>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">开始时间</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control startTime"  placeholder="开始时间" value="${param.startTime}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">结束时间</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control endTime"  placeholder="结束时间" value="${param.endTime}">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <input type="button" class="btn-query btn btn-primary form-control" value="查询历史数据">
                        </div>
                    </div>

                </form>
            </div>
            <div class="well col-md-10 col-md-offset-1">
                <div class="alert">
                    <p>总共<label class="text-primary"><b>${count}</b></label>记录
                    <p>
                        ${empty repeatList ? '无' : ''}

                        重复时间记录
                        <c:if test="${ not empty repeatList}">
                            <c:forEach var="r" items="${repeatList}">
                                <lable> ${r}</lable>
                            </c:forEach>
                        </c:if>
                    </p>
                </div>
                <table class="table table-striped table-hover ">
                    <thead>
                    <tr>
                        <th>TagID</th>
                        <th>温度</th>
                        <th>湿度</th>
                        <th>记录时间</th>
                    </tr>
                    </thead>
                    <c:forEach var="d" items="${dataList}">
                        <tbody>
                        <tr>
                            <td>${d.tagId}</td>
                            <td>${d.temperature}℃</td>
                            <td>${d.humidity}%</td>
                            <td>${d.recordTime}</td>
                        </tr>
                        </tbody>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
<script src="/assets/js/HistoryData.js"></script>
<script>
    var historyData = new HistoryData();
</script>
</body>
</html>