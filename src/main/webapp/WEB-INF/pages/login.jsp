<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017\9\7 0007
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <title>查询后台数据的登录页面</title>
</head>
<body>
<div class="container">
    <div class="topdiv"></div>
    <h1 class="text-center"><i class="fa fa-home"></i>&nbsp;欢迎进入后台数据管理平台</h1>
    <div class=" col-lg-offset-2 col-lg-8 panel well">
        <div class="panel-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="userName" class="col-sm-2 control-label" >用户名</label>
                    <div class="col-sm-9 input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                        <input type="text" class="form-control" id="userName" placeholder="用户名"><span id="a"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="col-sm-2 control-label" >密码</label>
                    <div class="col-sm-9 input-group">
                        <div class="input-group-addon"><i class="fa fa-eye"></i></div>
                        <input type="password" class="form-control" id="pass" placeholder="密码"><span id="b"></span>
                    </div>
                </div>
                <%--<div class="form-group">
                    <div class="check-feedback hide alert alert-warning col-sm-9 col-sm-offset-2"><strong><i class="fa fa-exclamation-triangle"></i>警告 </strong>&nbsp;请确保用户名，密码信息完整！</div>
                    <div class="login-feedback hide alert alert-danger col-sm-9 col-sm-offset-2"><strong><i class="fa fa-exclamation-triangle"></i>错误</strong>&nbsp;用户名,密码不正确！</div>
                </div>--%>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                        <button  id="btn-t"  class="btn btn-success btn-lg btn-block btn-submit" data-loading-text="登陆中..." type="button"><i class="fa fa-sign-in"></i>&nbsp;登  录</button>
                        <span id="c"></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
<script src="/assets/js/user.js"></script>
<script>
    var user=new User();
</script>
</body>
</html>
