<%--suppress ALL --%>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017\8\28 0028
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<html>
<head>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <title>最新探头列表</title>
</head>

<body >
<% request.setAttribute("active","3");%>
<%@ include file="navbar.jsp"%>
<div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><p>默认刷新时间是:1分钟</p></strong><p><b>1.显示蓝色</b>(记录时间与当前时间的差值在${param.timeout==null?"10":param.timeout}分钟内)<b>2.显示黄色</b>(记录时间与当前时间的差值在${param.timeout==null?"10":param.timeout}分钟外)</p>
</div>
<div id="td1" class='row'>

</div>

<script id="entry-template" type="text/x-handlebars-template">
    {{#each list}}
    <div  class="container col-lg-2 col-md-2 col-sm-2">
        <div class="{{time}}"}>
            <div class="panel-heading">
                <h3 class="panel-title">{{tagId}}</h3>
            </div>
            <div class="panel-body">
                <div>温度:{{temperature}}</div>
                <div>湿度:{{humidity}}</div>
                <div>记录时间:{{startTime}}</div>
            </div>
        </div>
    </div>
    {{/each}}
</script>


<jsp:include page="footer.jsp"></jsp:include>
<script src="/assets/js/data.js"></script>
<script>
    var data = new Data();
</script>

</body>

</html>
