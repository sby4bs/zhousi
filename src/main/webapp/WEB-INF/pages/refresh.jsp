<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
 <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <title>查询探头最新数据</title>

     <!-- Bootstrap -->
     <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
     <![endif]-->
 </head>
 <body data-timeout=${param.timeout}>
   <% request.setAttribute("active","2");%>
   <%@ include file="navbar.jsp"%>

   <div class="container col-lg-12 col-md-12 col-sm-12">
     <!-- Default panel contents -->
       <div class="alert well">
           探头最新温湿度记录表：蓝色（${param.timeout == null? "10":param.timeout}分钟以内数据），黄色（${param.timeout == null? "10":param.timeout}分钟之前数据）；<br>
           默认刷新时间：1 minute；
       </div>
       <div id = 'list'></div>
   </div>
 </div>

<jsp:include page="footer.jsp"></jsp:include>

<script src="/assets/js/refresh.js"></script>

<script type="application/template" id="real_template">
    {{#each list}}
     <div class="col-lg-2 col-md-2 col-sm-2">
         <div class="panel panel-{{color}}">
             <div class="panel-heading">
                 <h3 class="panel-title"><b><span class="glyphicon glyphicon-phone"></span>{{tagId}}</b></h3>
             </div>
             <div class="panel-body">
                 温度: {{temperature}}<br>
                 湿度: {{humidity}}<br>
                 时间: {{recordTimeStr}}
             </div>
         </div>
     </div>
    {{/each}}
</script>

<script>
    var refresh = new Refresh();
</script>

</body>
</html>
