var User = function(){
    this.init();
}
User.prototype.init = function(){
    $("#btn-t").click(this.toLogin.bind(this));

}
User.prototype.toLogin=function(){
    var admincode = $("#userName").val();
    //console.log(admincode);
    var password = $("#pass").val();
    if(!admincode){
        $("span").empty();
        $("#a").html("未填写账号!")
        return;

    }
    if(!password){
        $("span").empty();
        $("#b").html("未填写密码!")
        return;
    }
    var data = {"admincode":admincode, "password":password};
    $.getJSON(URLHelper.loginurl(),data,function(result){
        //console.log(result);
        if(result.success == 0){
            $("span").empty();
            $("#c").html(result.message);
        } else if(result.success == 1){
            $("span").empty();
            $("#c").html(result.message);
        } else{
            location.href = URLHelper.dataurl();
        }

    });
}