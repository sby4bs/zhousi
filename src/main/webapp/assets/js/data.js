
var Data=function(){
    this.init();
};

Data.prototype.init=function () {
    //当页面加载时就会执行下面事件
    this.findData();
    setInterval(this.findData.bind(this),60*1000);
};

Data.prototype.findData = function () {
    URLHelper.ajax(URLHelper.finddataurl(), 'GET', null, function(result){
        $("#td1").empty();
        var source = $("#entry-template").html();
        var template = Handlebars.compile(source);
        var context = {'list':result};
        var html = template(context);
        $("#td1").html(html);
    });

    var that = this;
    Handlebars.registerHelper("time",function(){

        var date = this.startTime;
        var time1 = (new Date(date)).getTime();
        var time2 = (new Date()).getTime();
        var number = Math.abs(time1-time2);
        var n = that.getTime("timeout");
        var time3;
        if(n == null){
            time3 = 10*60*1000;
        }else{
            time3 = n*60*1000;
        }
        if(number<time3){
            return "panel panel-primary";

        }else{
            return "panel panel-warning";

        }

    });
};

Data.prototype.getTime=function(timeout){
    var reg = new RegExp("(^|&)"  +  timeout  +  "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
};

       /*for(var i=0;i<result.length;i++){
            var data=result[i];
            var date=data.startTime;
            var time1=(new Date(date)).getTime();
            var time2=(new Date()).getTime();
            var n=that.getTime("timeout");
            var time3;
            if(n==null){
                time3=10*60*1000;
            }else{
                time3=n*60*1000;}
            var number=Math.abs(time1-time2);

           // console.log(time3);
            //console.log(number);
            /*if(number<time3){
                //console.log(result[0]);
                $("#td").append(
                    "<div class='col-lg-2 col-md-2 col-sm-2' >"+
                    "<div class='panel panel-primary ' >"+
                    "<div class='panel-heading' ><h3 class='panel-title' >"+ "<a href=/?tagId="+data.tagId+"><span class='glyphicon glyphicon-phone'></span>"+data.tagId+"</a>"
                    +"</h3></div>"+
                    "<div class='panel-body'><div>"+"温度:"+data.temperature
                    +"</div><div>"+"湿度:"+data.humidity
                    +"</div><div>"+"记录时间:"+data.startTime
                    +"</div></div>"
                    +"</div>"
                    +"</div>"
                );
            }else{
                $("#td").append(
                    "<div class='col-lg-2 col-md-2 col-sm-2' >"+
                    "<div class='panel panel-warning'  >"+
                    "<div class='panel-heading' ><h3 class='panel-title' >"+"<a href=/?tagId="+data.tagId+"><span class='glyphicon glyphicon-phone'></span>"+data.tagId+"</a>"
                    +"</h3></div>"+
                    "<div class='panel-body'><div>"+"温度:"+data.temperature
                    +"</div><div>"+"湿度:"+data.humidity
                    +"</div><div>"+"记录时间:"+data.startTime
                    +"</div></div>"
                    +"</div>"
                    +"</div>"
                );

            }
        }*/

