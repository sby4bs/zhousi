/**
 * Created by zhaoyou on 30/08/2017.
 */

var HistoryData = function() {
    this.init();
};

HistoryData.prototype.init = function() {
    $('.startTime, .endTime').datetimepicker({
        viewMode:'days',
        format:'YYYY-MM-DD HH:mm:00',
        locale: 'zh-cn'
    });
    $('.btn-query').click(this.query.bind(this));
};

HistoryData.prototype.query = function() {
    var startTime = $('.startTime').val();
    var endTime = $('.endTime').val();
    var tagId = $('input:radio[name=tagId]:checked').val();

    if (!tagId || !startTime || !endTime) { alert('请选择设备和时间范围!');return;}
    window.location.href = URLHelper.historyDataUrl(tagId, startTime, endTime);
};
