/**
 * Created by zhail on 31/08/2017.
 */

var Refresh = function () {
    this.init();
};

Refresh.prototype.init= function () {
    Handlebars.registerHelper('color', function() {
        var retime = Date.parse(this.recordTime);
        var systime = new Date().getTime();
        var timeout = $('body').attr('data-timeout');
        if(systime-retime>= (timeout? timeout*60000:600000)){
            return "warning";
        };
        return "primary";
    });

        this.loadData();
        setInterval(this.loadData.bind(this),60*1000);

};

Refresh.prototype.loadData = function () {
    URLHelper.ajax(URLHelper.loaddataurl(), "GET", null, function(data){
        var source = $('#real_template').html();
        var template = Handlebars.compile(source);
        var htmlString = template({'list': data});
        $('#list').html(htmlString);
        $('.panel-title').find('b').hover(function () {
            $(this).css('cursor','pointer').css("text-decoration","underline");
        },function () {
            $(this).css("text-decoration","none");
        });
        $('.panel-title').click(function () {
            location.href =  '/?tagId=' + $(this).text();
        });
    });

};
        /*var trs = '';
        for(i = 0; i < rs.length; i ++){
            var d = rs[i];
            var retime = Date.parse(d.recordTime);
            var systime = new Date().getTime();
            var timeout = $('body').attr('data-timeout');
            if(systime-retime>= (timeout? timeout*60000:600000)){
                trs += '<div class="col-lg-2 col-md-2 col-sm-2"><div class="panel panel-warning"><div class="panel-heading"><h3 class="panel-title"><b><span class="glyphicon glyphicon-phone"></span>' + d.tagId
                    + '</b></h3></div><div class="panel-body">温度:' + d.temperature
                    + '<br>湿度:' + d.humidity
                    + '<br>时间:' + d.recordTimeStr
                    + '</div></div></div>';
            }else{
                trs += '<div class="col-lg-2 col-md-2 col-sm-2"><div class="panel panel-primary"><div class="panel-heading"><h3 class="panel-title"><b><span class="glyphicon glyphicon-phone"></span>' + d.tagId
                    + '</b></h3></div><div class="panel-body">温度:' + d.temperature
                    + '<br>湿度:' + d.humidity
                    + '<br>时间:' + d.recordTimeStr
                    + '</div></div></div>';
            }
        }
        $('#list').html(trs);*/

