package com.thermoberg.zhousi.service;

import com.thermoberg.zhousi.model.Admin;
import com.thermoberg.zhousi.model.Device;
import com.thermoberg.zhousi.model.HistoryData;
import com.thermoberg.zhousi.model.LastDate;
import org.springframework.http.HttpRequest;

import java.util.List;

/**
 * Created by zhaoyou on 07/08/2017.
 */
public interface DeviceService {
    public List<Device> findAll();
    public List<HistoryData> findData(String tagId, String startTime, String endTime);
    public List<String> findRepeatData(String tagId, String startTime, String endTime);
    public Admin findAdminPassword(String Code,String Password);

    public List<HistoryData> findData();

    public  List<LastDate> findLastRecord();

}
