package com.thermoberg.zhousi.service.impl;

import com.thermoberg.zhousi.dao.DeviceDao;
import com.thermoberg.zhousi.model.Admin;
import com.thermoberg.zhousi.model.Device;
import com.thermoberg.zhousi.model.HistoryData;
import com.thermoberg.zhousi.model.LastDate;
import com.thermoberg.zhousi.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zhaoyou on 07/08/2017.
 */
@Service
public class DeviceServiceImpl implements DeviceService{

    @Autowired
    private DeviceDao deviceDao;

    @Override
    public List<Device> findAll() {
        return deviceDao.findDevice();
    }

    @Override
    public List<HistoryData> findData(String tagId, String startTime, String endTime) {
        return deviceDao.findHistoryData(tagId, startTime, endTime);
    }

    @Override
    public List<String> findRepeatData(String tagId, String startTime, String endTime) {
        return deviceDao.findRepeatData(tagId, startTime, endTime);
    }

    @Override
    public Admin findAdminPassword(String Code,String adminPassword) {
        if(Code==null||Code.isEmpty()){
            System.out.println("您输入的账号有误");
        }
        Admin a=deviceDao.findAdminPassword(Code);
        return  null;
    }

    @Override
    public List<HistoryData> findData() {
        List<HistoryData> list= deviceDao.findHistoryData();
        return list;
	}
	
    @Override
    public  List<LastDate> findLastRecord(){
        return deviceDao.findLastRecord();
    }


}



