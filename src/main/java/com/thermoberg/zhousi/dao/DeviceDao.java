package com.thermoberg.zhousi.dao;

import com.thermoberg.zhousi.model.Admin;
import com.thermoberg.zhousi.model.Device;
import com.thermoberg.zhousi.model.HistoryData;
import com.thermoberg.zhousi.model.LastDate;

import java.util.List;

/**
 * Created by zhaoyou on 07/08/2017.
 */
public interface DeviceDao {
    public List<Device> findDevice();

    public List<HistoryData> findHistoryData(String tagId, String startTime, String endTime);

    public List<String> findRepeatData(String tagId, String startTime, String endTime);

    public Admin findAdminPassword(String adminCode);

    public List<HistoryData> findHistoryData();

    public List<LastDate> findLastRecord();

}
