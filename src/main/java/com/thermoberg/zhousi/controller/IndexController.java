package com.thermoberg.zhousi.controller;

import com.thermoberg.zhousi.model.HistoryData;
import com.thermoberg.zhousi.model.LastDate;
import com.thermoberg.zhousi.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by zhaoyou on 07/08/2017.
 */
@Controller
public class IndexController {

    @Autowired
    private DeviceService deviceService;

    @RequestMapping("/")
    public String index(Model model) {
            model.addAttribute("deviceList", deviceService.findAll());
            return "index";
    }

    @RequestMapping("/v/query")
    public String query(@RequestParam String tagId,
                        @RequestParam String startTime,
                        @RequestParam String endTime, Model model) {
        List<HistoryData> list = deviceService.findData(tagId, startTime, endTime);
        model.addAttribute("dataList", list);
        model.addAttribute("count", list == null ? 0 : list.size());
        model.addAttribute("repeatList", deviceService.findRepeatData(tagId, startTime, endTime));
        model.addAttribute("deviceList", deviceService.findAll());
        return "index";
    }
    @RequestMapping("/data")
    public String toData(){
        return "data";
    }

    @RequestMapping("/findData")
    @ResponseBody
    public Object findHistoryData(){
        List<HistoryData> list = deviceService.findData();
        return list;
    }
    @RequestMapping("/v/queryNew")
    public String queryNew(HttpServletRequest req){
        return "refresh";
    }

    @RequestMapping("/v/new")
    @ResponseBody
    public List<LastDate> upData(HttpServletRequest req){
        return deviceService.findLastRecord();
    }
    @RequestMapping("/test.do")
    public String test(){

        return "test";
    }






}
