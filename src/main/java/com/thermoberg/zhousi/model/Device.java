package com.thermoberg.zhousi.model;

/**
 * Created by zhaoyou on 07/08/2017.
 */
public class Device {
    private Integer id;
    private String tagId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", tagId='" + tagId + '\'' +
                '}';
    }
}
