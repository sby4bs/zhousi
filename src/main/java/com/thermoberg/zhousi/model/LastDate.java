package com.thermoberg.zhousi.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class LastDate implements Serializable{
    private String tagId;
    private Double temperature;
    private Double humidity;
    private Timestamp recordTime;
    private String recordTimeStr;

    public LastDate(){}

    public LastDate(String tagId, Double temperature, Double humidity, Timestamp recordTime, String recordTimeStr) {
        this.tagId = tagId;
        this.temperature = temperature;
        this.humidity = humidity;
        this.recordTime = recordTime;
        this.recordTimeStr = recordTimeStr;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Timestamp getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Timestamp recordTime) {
        this.recordTime = recordTime;
    }

    public String getRecordTimeStr() {
        return recordTimeStr;
    }

    public void setRecordTimeStr(String recordTimeStr) {
        this.recordTimeStr = recordTimeStr;
    }

    @Override
    public String toString() {
        return "LastDate{" +
                "tagId='" + tagId + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", recordTime=" + recordTime +
                ", recordTimeStr='" + recordTimeStr + '\'' +
                '}';
    }
}
