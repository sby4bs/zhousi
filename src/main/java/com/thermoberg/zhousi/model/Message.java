package com.thermoberg.zhousi.model;

public class Message {
    private String message;
    private Integer success;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", success=" + success +
                '}';
    }
}