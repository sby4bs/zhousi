package com.thermoberg.zhousi.model;

import java.sql.Timestamp;

/**
 * Created by zhaoyou on 07/08/2017.
 */
public class HistoryData {
    private Integer id;
    private String tagId;
    private Double temperature;
    private Double humidity;
    private Timestamp recordTime;
    private Timestamp uploadTime;
    private  String startTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Timestamp getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Timestamp recordTime) {
        this.recordTime = recordTime;
    }

    public Timestamp getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Timestamp uploadTime) {
        this.uploadTime = uploadTime;
    }

    @Override
    public String toString() {
        return "HistoryData{" +
                "id=" + id +
                ", tagId='" + tagId + '\'' +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", recordTime=" + recordTime +
                ", uploadTime=" + uploadTime +
                ", startTime='" + startTime + '\'' +
                '}';
    }
}
