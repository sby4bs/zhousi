package com.thermoberg.zhousi.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by zhaoyou on 11/11/14.
 */

@Component
public class LoginToken {


    @Autowired
    private CrytoDemo crytoDemo;

    public String buildToken(String admincode,String password) {
        try {
            Long now = new Date().getTime() + (1000 * 60 * 60 * 2);
            return crytoDemo.textToEncrypt( admincode +":"+ password +":"  + now);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("加密登陆value失败！");
        }
    }

    public String[] getUserInfo(String token) {
        try {
            String plainText = crytoDemo.decrypt(token);
            String[] items = plainText.split(":");
            Long now = new Date().getTime();

            // 判断用户账号是否过期。
            if (now > Long.parseLong(items[2])) {
                return null;
            }
            return items;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
